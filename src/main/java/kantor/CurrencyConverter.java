package kantor;

import org.decimal4j.util.DoubleRounder;

import java.text.DecimalFormat;

public class CurrencyConverter {

    private String name;
    private double exchange;


    private final DecimalFormat df2 = new DecimalFormat("#.##");

    public CurrencyConverter(String name, double exchange) {
        this.name = name;
        this.exchange = exchange;
    }

    public String getName() {
        return name;
    }

    public double getExchange() {
        return exchange;
    }

    public double convertEuroToZl(double value){

//        return  value * exchange;
        return DoubleRounder.round(value * exchange,3);

    }

    public double convertZlToEur(double value){

        return DoubleRounder.round((value/exchange), 3);
//        return Double.parseDouble(df2.format(value/exchange));
    }


    public static void main(String[] args) {


        CurrencyConverter currencyConverter1= new CurrencyConverter("Kantor Baśka", 3.42);
        CurrencyConverter currencyConverter2= new CurrencyConverter("Kantor Zbigniew", 3.18);

        System.out.println(currencyConverter1.convertEuroToZl(100));
        System.out.println(currencyConverter1.convertZlToEur(500));

        System.out.println(currencyConverter2.convertEuroToZl(100));
        System.out.println(currencyConverter2.convertZlToEur(500));


    }

}
