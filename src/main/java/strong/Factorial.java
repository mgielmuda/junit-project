package strong;

import java.math.BigInteger;

public class Factorial {

    public static long countFactorialForSelectedNumber(int inputNumber){
        int factorial;
        factorial=1;

        for (int i=1; i<= inputNumber; i++){
            factorial = factorial *i;
        }
        return factorial;
    }

   public static BigInteger countFactorialForSelectedNumber2(int inputNumber){
        BigInteger f = BigInteger.valueOf(1);
        BigInteger f1 = new BigInteger("1");
       for (int i=1; i<= inputNumber; i++){
           f = f.multiply(BigInteger.valueOf(i));
       }
       return f;

   }


    public static void main(String[] args) {

//        System.out.println(countFactorialForSelectedNumber(5));
        System.out.println(countFactorialForSelectedNumber(4));
        System.out.println(countFactorialForSelectedNumber(8));

        System.out.println(countFactorialForSelectedNumber2(5));
        System.out.println(countFactorialForSelectedNumber2(8));
        System.out.println(countFactorialForSelectedNumber2(20));




    }
}


