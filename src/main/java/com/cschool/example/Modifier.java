package com.cschool.example;

@FunctionalInterface

public interface Modifier {
    String modify(String string);
}
