package com.cschool.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class LambdaExample3 {


    public static void main(String[] args) {

        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        integers.forEach(element -> {
            if (element % 2 == 0) {
//                System.out.println(element);
            }
        });


        List<String> names = new ArrayList<>();
        names.add("Jack");
        names.add("John");
        names.add("Jasmine");

        List<String> modifiedNames = new ArrayList<>();
        Random random = new Random();

        for (String str1: names) {
            String modifiedName = modifyMetod(str1,n -> n +" "+ random.nextInt(11));

//            modifiedNames.add(str + " "+ random.nextInt(11));
            modifiedNames.add(modifiedName);
        }

        System.out.println(modifiedNames);


    }

    private  static  String modifyMetod(String str, Modifier modifier){
        return modifier.modify(str);
    }
}
