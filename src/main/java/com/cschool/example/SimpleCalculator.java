package com.cschool.example;

public class SimpleCalculator {

    public static  int add(int a, int b){
        return a+b;
    }

    public static  int multiply(int a, int b){
        return a*b;
    }


    public static int divide(int a, int b){

        if (b==0){
            throw new IllegalArgumentException("Can't divide by 0!");//unchect exeption i nie trzeba róbić tray/cach
        }

        return a/b;
    }

    public static void main(String[] args) {

        System.out.println("Simple Calculator");
    }
}