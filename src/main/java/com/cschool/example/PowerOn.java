package com.cschool.example;

@FunctionalInterface

public interface PowerOn {
        void activate();
}
