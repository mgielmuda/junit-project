package com.cschool.example;

public class LambdaExample {

    public static void main(String[] args) {


        SumImpl sumImpl= new SumImpl();
//        com.cschool.example.Sum sumImpl2= new com.cschool.example.SumImpl(); //poliformizm


        Sum sumInstance = new Sum() {// podspodem java tworzy klacę to sumInstance wskazuje na tą klasę tworzoną podspodem która implementuje dany interfejs
            @Override// klasa anonimowa zrobiona w locie- nie tworzy z boku w folderze javy oddzielnej klasy którą możemy podejrzeć- to jest taka sama funkcjonalność jak SumImp
            public int calculate(int a, int b) {
                return 0;
            }
        };

        Sum sumInstance2 = (a,b)->a + b + 10;
        Sum sumInstance3 = (x,y)-> x + y + 8;
        // gdy usuwamy rzeczy które się powtarzają robimy lambdę np:
//            @Override
//            public int calculate



        System.out.println(sumImpl.calculate(2,6));
        System.out.println(sumInstance.calculate(2,6));
        System.out.println(sumInstance2.calculate(2,6));



    }
}
