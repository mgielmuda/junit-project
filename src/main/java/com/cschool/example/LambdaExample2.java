package com.cschool.example;

public class LambdaExample2 {

    public static void main(String[] args) {


        Sum sum1 = (a,b) -> a * b + 10;

        System.out.println(sum1.calculate(2,3));

        StringInter stringInter= string -> string.concat("abc"+ "abc".toUpperCase());


        StringInter stringInter2= string -> {
            String upperCase= string.toUpperCase();
            return string.concat(upperCase);
        };


        PowerOn powerOn = ()->{
            System.out.println(SimpleCalculator.add(2,6));
        } ;


        System.out.println(stringInter.metodString("words "));
        System.out.println(stringInter2.metodString("words "));
       powerOn.activate();




    }
}
