package circle;

public class Circle {

    private int r;


    public Circle(int r) {
        this.r = r;
    }

    public int getR() {
        return r;
    }

    public double surfaceArea(){

        return  Math.PI*(r^2);

    }
}
