package circle;


public class Roller{

    private int h;
    private Circle circle;

    public Roller(int h, Circle circle) {
        this.h = h;
        this.circle = circle;
    }

    public int getH() {
        return h;
    }

    public double surfaceAreaRoller(){
//2*pi*r^2+2*pi*r*H

        return  (2*circle.surfaceArea())+(Math.PI*circle.getR()*h);

    }

}
