package school.project;

import java.util.Objects;

public class Meal {

    private int price;
    private String name;

    public Meal(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public Meal(int price, String name) {
        this.price = price;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getDiscountprice(int discount){
        if (discount > price){
            throw new IllegalArgumentException("Discount is bigger than price : "+price);
        }
        return this.price - discount;
    }

    public static void main(String[] args) {
        Meal burger = new Meal(10);

        System.out.println(burger.getDiscountprice(4));

        Meal meal1 = new Meal(10);
        Meal meal2 = new Meal(10);
        Meal meal3 = new Meal(20);

        System.out.println(meal1.equals(meal2)); //true
        System.out.println(meal1.equals(meal3)); //fals

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Meal)) return false;
        Meal meal = (Meal) o;
        return getPrice() == meal.getPrice() &&
                Objects.equals(getName(), meal.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPrice(), getName());
    }
}
