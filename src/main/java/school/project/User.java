package school.project;

public class User {

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {

        if (name.length()>3) {
            this.name = name;
        }else{
            throw new IllegalArgumentException("Name is to short");
        }
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {

        if (age>18){
            this.age = age;
        }else{
            throw new IllegalArgumentException("Age is under 18!");
        }

    }
}
