package school.project;

import java.util.ArrayList;
import java.util.List;

public class Order {

    private List<Meal> meals = new ArrayList<>();

    //add posiłek do listy void

    public void  addMeal(Meal mealToAdd){

        meals.add(mealToAdd);

    }

     public void removeMealFromOrder (Meal mealToRemove){
        meals.remove(mealToRemove);
     }

    public List<Meal> getMeals() {
        return meals;
    }

    public void addMeal(List<Meal> mealList1) {

        meals.addAll(mealList1);
    }

    //getMeals --getter
}
