package school.project;

import com.cschool.example.Main;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;


import static org.junit.jupiter.api.Assertions.*;

class MealTest {

    @Test
    void shouldReturnDiscountPrice(){

        //given
        Meal meal = new Meal(10);

        //when
        int priceAfterDiscount = meal.getDiscountprice(4);

        //then
        assertEquals(6, priceAfterDiscount);
    }

    @Test
    void shouldThrowIllegalExeption(){

        //given
        Meal meal = new Meal(10);

        //when
        Executable lambda = ()-> meal.getDiscountprice(2);

        //then
        //1
//       assertThrows(IllegalArgumentException.class,  () -> {
//            meal.getDiscountprice(20);
//        });

        //2
        assertThrows(IllegalArgumentException.class, lambda);
    }

        @Test
        void  referenceToDiffrentObjectSchouldNotBeEqual(){
        //given
            Meal meal = new Meal(10);
            Meal meal2 = meal;
            Meal meal3 = new Meal(10);

            //when, then - patrzy czy to nie ten sam obiekt
//            assertNotSame(meal,meal2);
//            assertNotSame(meal,meal3);
            assertEquals(meal.getPrice(),meal3.getPrice());

        }


        //

    @Test
    void  twoMealsShouldBeEqualWhenPriceAndNameTheSame(){
        //given
        Meal meal = new Meal(10, "Burger");
        Meal meal2 = meal;
        Meal meal3 = new Meal(10, "Burger1");

        //when, then - patrzy czy to nie ten sam obiekt
//            assertNotSame(meal,meal2);
//            assertNotSame(meal,meal3);
        assertEquals(meal,meal3);

    }
}