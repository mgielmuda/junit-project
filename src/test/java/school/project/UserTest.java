package school.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


class UserTest {


    @Test
    void checkIfNewUserHaveValueOfNameNull(){

        User user = new User();

        assertThat(user.getName(), nullValue());
    }


    @Test
    void checkIfUserIsAdult(){

        User user = new User();

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            user.setAge(15);
        });
    }

    @ParameterizedTest
    @ValueSource(strings = {"jan", "xxx", "sdf"})
    void checkIfISMorThan3(String string) {

        User user= new User();


        Assertions.assertThrows(IllegalArgumentException.class, () ->{
            user.setName(string);
        });
    }


}