package school.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class AccountTest {

    @Test
    void nowlyCreatedAccountSchouldNotBeActive() {
        Account account = new Account();
        assertFalse(account.isActivate(),
                "Check is new account is not activ");
        assertThat(account.isActivate(), equalTo(false));
        assertThat(account.isActivate(), is(false));
    }

    @Test
    void nowlyCreatedAccountSchouldBeActiveAfterActivation() {

        //given
        Account account = new Account();

        //when
        account.activate();

        //then
        assertTrue(account.isActivate(),
                "Check is new account is not activ");
    }


    @Test
    void newlyCreatedAccountShouldHaveDefaultDeliveryAddressEqualToNull() {

        //given
        Account account = new Account();

        //then
        assertNull(account.getGetDefaultDeliveredAddress());
        assertThat(account.getGetDefaultDeliveredAddress(), nullValue());
    }

    @Test
    void deliveryAdressSchouldNotBeNullAfrterBeeingSet() {
        Account account = new Account();
        assertThat(account.getGetDefaultDeliveredAddress(), notNullValue());


    }

    //    public void setEmailAddress(String emailAddress) {
////
////        // chceck if constains @
////
////        if (emailAddress.contains("@")) {
////            this.emailAddress = emailAddress;
////        }else{
////            throw new IllegalArgumentException("Email must contatin @ character")
////        }
////    }
    @ParameterizedTest
    @ValueSource(strings = {"jon@wp.pl", "aaaaaaa@gmail.com", "shhh.gmail.com"})
    void checkIfGivenEmailAddressIsCorrect(String string) {

        //given
        Account account = new Account();

        //when
        account.setEmailAddress(string);

        //then
        assertThat(account.getEmailAddress(), notNullValue());
    }


    @ParameterizedTest
    @ValueSource(strings = {"jon@wp.pl", "aaaaaaa@gmail.com", "shhh.gmail.com"})
    void checkIfGivenEmailAddressIsCorrectLength(String string) {

        //given
        Account account = new Account();

        //when
        account.setEmailAddress(string);

        //then
        assertThat(account.getEmailAddress(), hasLength(10));
    }


    @Test
    void checkIfMethodThrowIllegalExceptionIfEmailNotCorrect() {

        //given
        Account account = new Account();


//        ()-> meal.getDiscountprice(2);

        //then
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            account.setEmailAddress("asdsad");
        });
    }
}