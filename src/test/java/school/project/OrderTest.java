package school.project;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    @Test
    void testAssertArraysEquals(){

        //given
        int[] ints1 = {1,2,3};
        int[] ints2 = {1,2,3};

        //then
        assertArrayEquals(ints1,ints2);

    }

    @Test
    void mealShouldBeEmptyAfterCreationOfOrder(){
        //given
        Order order = new Order();

        //then - 4 sposoby sprawdzenia czy kolekcja jest pusta
        assertThat(order.getMeals(), empty());
        assertThat(order.getMeals().size(), equalTo(0));
        assertThat(order.getMeals(), hasSize(0));
        assertThat(order.getMeals(), emptyCollectionOf(Meal.class));
    }


    @Test
    void addingMealToOrderShouldCreateOrderSize(){
        //given
        Order order = new Order();

//        when
        order.addMeal(new Meal(10,"Banan"));

        //then
        assertThat(order.getMeals().size(), equalTo(1));

    }

    @Test
    void addingMealToOrderShouldIncreaseOrderListSize(){

        Meal meal1= new Meal(10,"Kebab");
        Meal meal2= new Meal(12,"Pizza");

        Order order= new Order();
        order.addMeal(meal1);

        assertThat(order.getMeals(), hasItem(meal1));
        assertThat(order.getMeals().get(0).getPrice(), equalTo(10));

    }

    @Test
    void removingMealFromOrderShouldDecreaseOrderSize() {
        Meal meal1 = new Meal(10, "Kebab");

        Order order = new Order();
        order.addMeal(meal1);
        order.removeMealFromOrder(meal1);

        assertThat(order.getMeals(), not(contains(meal1)));
        assertThat(order.getMeals(),empty());
        assertThat(order.getMeals(),hasSize(0));

    }

    @Test
    void mealsShouldBeInCorrectOrderAfterAddingThemToOrder (){

        Meal meal1= new Meal(10,"Kebab");
        Meal meal2= new Meal(12,"Pizza");
        Meal meal3= new Meal(12,"Pizza");

        Order order = new Order();
        order.addMeal(meal1);
        order.addMeal(meal2);
        order.addMeal(meal3);

        assertThat(order.getMeals(), contains(meal1,meal2,meal3)); // elementy muszą być dodane w tej samej kolejnosci

        assertThat(order.getMeals(), containsInAnyOrder(meal2,meal1, meal3)); // elementy w dowolnej kolejnosci

        assertThat(order.getMeals(), hasItems(meal3, meal1 )); //czy zawiera
        assertThat(order.getMeals(), hasItem(meal3 )); //czy zawiera
    }


    @Test
    void testIfTwoOrdersAreTheSame(){

        Meal meal1= new Meal(10,"Kebab");
        Meal meal2= new Meal(12,"Pizza");
        Meal meal3= new Meal(8,"Pierogi");

        List<Meal> mealList1 = Arrays.asList(meal1,meal2);
        List<Meal> mealList2 = Arrays.asList(meal1,meal3);
        List<Meal> mealList3 = Arrays.asList(meal1,meal3);
        List<Meal> mealList4 = Arrays.asList(meal2,meal3);

        Order order1 = new Order();
        Order order2= new Order();

        order1.addMeal(mealList3);
        order2.addMeal(mealList2);
//        order1.addMeal(meal3);

        assertThat(order1.getMeals(), is(order2.getMeals()));
    }
}