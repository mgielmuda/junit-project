package strong;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigInteger;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FactorialTest {

    @ParameterizedTest
    @MethodSource("factorialProvider")
    void factorialSchouldReturnCorrectValue(long value, int number){

        long valueOfFactorial = Factorial.countFactorialForSelectedNumber(number);
       assertEquals(value,valueOfFactorial);

//        Assertions.assertEquals(15511210043330985984000000,Factorial.countFactorialForSelectedNumber2(5));
//        Assertions.assertEquals(15511210043330985984000000,Factorial.countFactorialForSelectedNumber2(8));
//        Assertions.assertEquals(15511210043330985984000000,Factorial.countFactorialForSelectedNumber2(20));
    }


    private  static Stream factorialProvider(){

        return Stream.of(
                Arguments.of(24l, 4),
                Arguments.of(40320l, 8)

        );
    }


    @ParameterizedTest
    @MethodSource("factorialProvider2")
    void factorialSchouldReturnCorrectValue2(BigInteger value, int number){

        BigInteger valueOfFactorial = Factorial.countFactorialForSelectedNumber2(number);
        assertEquals(value,valueOfFactorial);

//        factorialSchouldReturnCorrectValue2(5, 2);

//        Assertions.assertEquals(15511210043330985984000000,Factorial.countFactorialForSelectedNumber2(5));
//        Assertions.assertEquals(15511210043330985984000000,Factorial.countFactorialForSelectedNumber2(8));
//        Assertions.assertEquals(15511210043330985984000000,Factorial.countFactorialForSelectedNumber2(20));
    }


    private  static Stream factorialProvider2(){

        return Stream.of(
                Arguments.of(new BigInteger("120"), 5),
                Arguments.of(new BigInteger("40320"), 8),
                Arguments.of(new BigInteger("2432902008176640000"), 20)

        );
    }

}