import com.cschool.example.SimpleCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SimpleCalculatorTest {

    @Test
    void add() {
        Assertions.assertEquals(4, SimpleCalculator.add(2,2));
       // assertEquals(4, com.cschool.example.SimpleCalculator.add(2,2));
    }


//    @Disabled// blokuje wykonanie testu poniżej
    @Test
    void multiply() {
        Assertions.assertEquals(10, SimpleCalculator.multiply(2,5));
    }

    @Test
    void divideExeption() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->{
            SimpleCalculator.divide(5,0);
        });
    }

    @Test
    void divide() {
        Assertions.assertEquals(2, SimpleCalculator.divide(10,5));
        // assertEquals(4, com.cschool.example.SimpleCalculator.add(2,2));
    }
}