package kantor;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CurrencyConverterTest {

    CurrencyConverter currencyConverter;


    @BeforeAll
    static void initializedGlobal() {
        System.out.println("@BeforeAll");
    }

    @BeforeEach
        // przed każdym testem tworzy obiekt zgodnie z poniższym
    void initializeConverter() {
        currencyConverter = new CurrencyConverter("Golden", 4.45);
        System.out.println("@Before Each");
    }


    @Test
    void checkCorectExchangeZlToEuro() {

        assertEquals(445, currencyConverter.convertEuroToZl(100));

    }

    @Test
    void checkCorectExchangeEuroToZl() {
        //given
        currencyConverter = new CurrencyConverter("test", 3.42);

        //when
        int amouthOfEuro = 500;

        //then
        assertEquals(146.199, currencyConverter.convertZlToEur(amouthOfEuro));

    }

}


